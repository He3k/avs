#include <fstream>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#define RAM 0
#define HDD 1
#define FLASH 2
#define CHAR_T 1
#define INT_T 2
#define FLOAT_T 4
#define DOUBLE_T 8
using namespace std;
int main(int argc, char **argv) {
  srand(time(NULL));
  struct timespec mt1, mt2;
  if (argc < 7) {
    printf("Not enough arguments\n");
    return 0;
  }
  int count = atoi(argv[6]);
  char MemoryType;
  if (strcmp(argv[2], "RAM") == 0) {
    MemoryType = RAM;
  } else if (strcmp(argv[2], "HDD") == 0) {
    MemoryType = HDD;
  } else if (strcmp(argv[2], "FLASH") == 0) {
    MemoryType = FLASH;
  } else {
    printf("Error enter MemoryType - %s\n", argv[2]);
    return 0;
  }
  int BlockSize;
  if (strcmp(argv[3], "-b") == 0) {
    BlockSize = atoi(argv[4]);
  } else if (strcmp(argv[3], "-k") == 0) {
    BlockSize = atoi(argv[4]) * 1024;
  } else if (strcmp(argv[3], "-m") == 0) {
    BlockSize = atoi(argv[4]) * 1024 * 1024;
  } else {
    printf("Error enter DataType - %s\n", argv[3]);
    return 0;
  }
  char ElementType[] = "double";
  int BufferSize;
  int LaunchNum = 0;
  char Timer[] = "clock_gettime()";
  double AbsError = 1;
  long int tt;
  long int unionTt = 0;
  if (MemoryType == RAM) {
    for (int k = 0; k <= 4; k++) {
      unionTt = 0;
      if (k == 0) {
        BufferSize = 8;
      } else if (k == 1) {
        BufferSize = 128 * 1024;
      } else if (k == 2) {
        BufferSize = 512 * 1024;
      } else if (k == 3) {
        BufferSize = 3 * 1024 * 1024;
      } else {
        BufferSize = 3 * 1024 * 1024 * 2;
      }
      int n_op = BlockSize / BufferSize;
      double *testBuf = (double *)malloc(BufferSize);
      for (int i = 0; i < BufferSize / sizeof(double); i++) {
        testBuf[i] = (double)i;
      }
      LaunchNum = 1;
      for (int i = 0; i < count; i++) {
        clock_gettime(CLOCK_REALTIME, &mt1);
        double **buf = (double **)malloc(n_op * sizeof(double *));
        for (int j = 0; j < n_op; j++) {
          buf[j] = (double *)malloc(BufferSize);
          memcpy(buf[j], testBuf, BufferSize);
        }
        clock_gettime(CLOCK_REALTIME, &mt2);
        tt = 1000000000 * (mt2.tv_sec - mt1.tv_sec) +
             (mt2.tv_nsec - mt1.tv_nsec);
        for (int j = 0; j < n_op; j++) {
          free(buf[j]);
        }
        free(buf);
        unionTt += tt;
        char *Timer = "clock_gettime";
        long int WriteTime = tt;
        long int AverageWriteTime = unionTt / (i + 1);
        double WriteBandwidth = (BlockSize / (1024 * 1024)) /
                                ((double)AverageWriteTime / 1000000000);
        double AbsError_write = 1;
        double RelError_write = (AbsError_write / (AverageWriteTime)) * 100;
        long int ReadTime = tt;
        long int AverageReadTime = unionTt / (i + 1);
        double ReadBandwidth = (BlockSize / (1024 * 1024)) /
                               ((double)AverageWriteTime / 1000000000);
        double AbsError_read = 1;
        double RelError_read = (AbsError_read / (AverageReadTime)) * 100;
        FILE *process;
        ofstream benchmark_output;
        benchmark_output.open("lab_3.csv", ios_base::app);
        benchmark_output << argv[2] << ";" << BlockSize << ";" << ElementType
                         << ";" << BufferSize << ";" << LaunchNum++ << ";"
                         << Timer << ";" << WriteTime << ";" << AverageWriteTime
                         << ";" << WriteBandwidth << ";" << AbsError_write
                         << ";" << RelError_write << ";" << ReadTime << ";"
                         << AverageReadTime << ";" << ReadBandwidth << ";"
                         << AbsError_read << ";" << RelError_read << ";"
                         << endl;
        benchmark_output.close();
      }
    }
  } else if (MemoryType == HDD) {
    BufferSize = 4 * 1024 * 1024;
    unionTt = 0;
    int n_op = BlockSize / BufferSize;
    double *testBuf = (double *)malloc(BufferSize);
    for (int i = 0; i < BufferSize / sizeof(double); i++) {
      testBuf[i] = (double)i;
    }
    LaunchNum = 1;
    for (int i = 0; i < count; i++) {
      clock_gettime(CLOCK_REALTIME, &mt1);
      clock_gettime(CLOCK_REALTIME, &mt2);
      tt = 1000000000 * (mt2.tv_sec - mt1.tv_sec) + (mt2.tv_nsec - mt1.tv_nsec);
      unionTt += tt;
      char *Timer = "clock_gettime";
      long int WriteTime = tt;
      long int AverageWriteTime = unionTt / (i + 1);
      double WriteBandwidth =
          (BlockSize / (1024 * 1024)) / ((double)AverageWriteTime / 1000000000);
      double AbsError_write = 1;
      double RelError_write = (AbsError_write / (AverageWriteTime)) * 100;
      long int ReadTime = tt;
      long int AverageReadTime = unionTt / (i + 1);
      double ReadBandwidth =
          (BlockSize / (1024 * 1024)) / ((double)AverageWriteTime / 1000000000);
      double AbsError_read = 1;
      double RelError_read = (AbsError_read / (AverageReadTime)) * 100;
      ofstream benchmark_output;
      benchmark_output.open("lab_3.csv", ios_base::app);
      benchmark_output << argv[2] << ";" << BlockSize << ";" << ElementType
                       << ";" << BufferSize << ";" << LaunchNum++ << ";"
                       << Timer << ";" << WriteTime << ";" << AverageWriteTime
                       << ";" << WriteBandwidth << ";" << AbsError_write << ";"
                       << RelError_write << ";" << ReadTime << ";"
                       << AverageReadTime << ";" << ReadBandwidth << ";"
                       << AbsError_read << ";" << RelError_read << ";" << endl;
      benchmark_output.close();
    }
  } else if (MemoryType == FLASH) {
    BufferSize = 4 * 1024 * 1024 * 10;
    unionTt = 0;
    int n_op = BlockSize / BufferSize;
    double *testBuf = (double *)malloc(BufferSize);
    for (int i = 0; i < BufferSize / sizeof(double); i++) {
      testBuf[i] = (double)i;
    }
    LaunchNum = 1;

    for (int i = 0; i < count; i++) {
      clock_gettime(CLOCK_REALTIME, &mt1);
      clock_gettime(CLOCK_REALTIME, &mt2);
      tt = 1000000000 * (mt2.tv_sec - mt1.tv_sec) + (mt2.tv_nsec - mt1.tv_nsec);
      unionTt += tt;
      char *Timer = "clock_gettime";
      long int WriteTime = tt;
      long int AverageWriteTime = unionTt / (i + 1);
      double WriteBandwidth =
          (BlockSize / (1024 * 1024)) / ((double)AverageWriteTime / 1000000000);
      double AbsError_write = 1;
      double RelError_write = (AbsError_write / (AverageWriteTime)) * 100;
      long int ReadTime = tt;
      long int AverageReadTime = unionTt / (i + 1);
      double ReadBandwidth =
          (BlockSize / (1024 * 1024)) / ((double)AverageWriteTime / 1000000000);
      double AbsError_read = 1;
      double RelError_read = (AbsError_read / (AverageReadTime)) * 100;
      ofstream benchmark_output;
      benchmark_output.open("lab_3.csv", ios_base::app);
      benchmark_output << argv[2] << ";" << BlockSize << ";" << ElementType
                       << ";" << BufferSize << ";" << LaunchNum++ << ";"
                       << Timer << ";" << WriteTime << ";" << AverageWriteTime
                       << ";" << WriteBandwidth << ";" << AbsError_write << ";"
                       << RelError_write << ";" << ReadTime << ";"
                       << AverageReadTime << ";" << ReadBandwidth << ";"
                       << AbsError_read << ";" << RelError_read << ";" << endl;
      benchmark_output.close();
    }
  }
  return 0;
}