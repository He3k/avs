#include <iostream>
#include <chrono>
#include <pthread.h>
#include <omp.h>

using namespace std;
double **result, **arr1, **arr2;
typedef struct someArgs
{
    int length;
    int num;
    int threadsNum;
};
void *DGEMM_BLAS_PT(void *arg)
{
    someArgs *a = (someArgs *)arg;
    for (int i = a->num; i < a->length; i += a->length)
    {
        for (int j = 0; j < a->length; j++)
        {
            for (int k = 0; k < a->length; k++)
            {
                result[i][j] += arr1[i][k] * arr2[k][j];
            }
        }
    }
    return 0;
}
void *createRandMatrixPT(void *args)
{
    someArgs *a = (someArgs *)args;
    for (int i = a->num; i < a->length; i += a->threadsNum)
    {
        result[i] = new double[a->length];
        for (int j = 0; j < a->length; j++)
        {
            result[i][j] = 0;
        }
    }
    for (int i = a->num; i < a->length; i += a->threadsNum)
    {
        arr1[i] = new double[a->length];
        for (int j = 0; j < a->length; j++)
        {
            arr1[i][j] = (double)rand() / RAND_MAX * 1000;
        }
    }
    for (int i = a->num; i < a->length; i += a->threadsNum)
    {
        arr2[i] = new double[a->length];
        for (int j = 0; j < a->length; j++)
        {
            arr2[i][j] = (double)rand() / RAND_MAX * 1000;
        }
    }
    return 0;
}
void DGEMM_BLAS_OMP(int length, int thread)
{
#pragma omp parallel for schedule(static) collapse(3)
    for (int i = 0; i < length; i++)
    {
        for (int j = 0; j < length; j++)
        {
            for (int k = 0; k < length; k++)
            {
                result[i][j] += arr1[i][k] * arr2[k][j];
            }
        }
    }
}
void createRandMatrixOMP(int length, int thread)
{
#pragma omp parallel for schedule(static)
    for (int i = 0; i < length; i++)
    {
        result[i] = new double[length];
        for (int j = 0; j < length; j++)
        {
            result[i][j] = 0;
        }
    }
#pragma omp parallel for schedule(static)
    for (int i = 0; i < length; i++)
    {
        arr1[i] = new double[length];
        for (int j = 0; j < length; j++)
        {
            arr1[i][j] = (double)rand() / RAND_MAX * 100;
        }
    }
#pragma omp parallel for schedule(static)
    for (int i = 0; i < length; i++)
    {
        arr2[i] = new double[length];
        for (int j = 0; j < length; j++)
        {
            arr2[i][j] = (double)rand() / RAND_MAX * 100;
        }
    }
}
int main()
{
    srand(time(NULL));
    int length, threadsNum;
    cout << "Enter size matrix: ";
    cin >> length;
    cout << "Enter number threads: ";
    cin >> threadsNum;
    arr1 = new double *[length];
    arr2 = new double *[length];
    result = new double *[length];

    pthread_t *thread = new pthread_t[threadsNum];
    someArgs *arg = new someArgs[threadsNum];

    for (int i = 0; i < threadsNum; i++)
    {
        arg[i].length = length;
        arg[i].num = i;
        arg[i].threadsNum = threadsNum;
    }
    for (int i = 0; i < threadsNum; i++)
    {
        if (pthread_create(&thread[i], NULL, createRandMatrixPT,
                           (void *)&arg[i]) != 0)
        {
            cout << "Error create threads";
            break;
        }
    }
    for (int i = 0; i < threadsNum; i++)
    {
        if (pthread_join(thread[i], 0) != 0)
        {
            cout << "Error wait threads";
        }
    }
    delete[] thread;
    thread = new pthread_t[threadsNum];

    auto start = chrono::steady_clock::now();
    for (int i = 0; i < threadsNum; i++)
    {
        if (pthread_create(&thread[i], NULL, DGEMM_BLAS_PT, (void *)&arg[i]) != 0)
        {
            cout << "Error create threads";
            break;
        }
    }
    for (int i = 0; i < threadsNum; i++)
    {
        if (pthread_join(thread[i], 0) != 0)
        {
            cout << "Error wait threads";
        }
    }
    auto end = chrono::steady_clock::now();
    chrono::duration<double> elapsed_seconds = end - start;
    cout << "Time exc (pthread): " << elapsed_seconds.count() << "s" << endl;
    omp_set_dynamic(0);
    omp_set_num_threads(threadsNum);

    createRandMatrixOMP(length, threadsNum);

    start = chrono::steady_clock::now();
    DGEMM_BLAS_OMP(length, threadsNum);
    end = chrono::steady_clock::now();
    elapsed_seconds = end - start;
    cout << "Time exc (OpenMP): " << elapsed_seconds.count() << " s"
         << endl;
}
